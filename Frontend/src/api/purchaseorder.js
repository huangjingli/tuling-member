import { axios } from '@/libs/api.request'

// #region  销售单
export const SearchPurchaseOrder = form => {
  return axios.post('api/PurchaseOrder/SearchPurchaseOrder', form)
}

export const SavePurchaseOrder = form => {
  return axios.post('api/PurchaseOrder/SavePurchaseOrder', form)
}
export const GetPurchaseOrder = id => {
  return axios.get('api/PurchaseOrder/GetPurchaseOrder/' + id)
}
export const GetPurchaseOrderAndPrint = id => {
  return axios.get('api/PurchaseOrder/GetPurchaseOrderAndPrint/' + id)
}

export const DeletePurchaseOrder = id => {
  return axios.post('api/PurchaseOrder/DeletePurchaseOrder/' + id)
}
export const CheckPurchaseOrder = form => {
  return axios.post('api/PurchaseOrder/CheckPurchaseOrder', form)
}
