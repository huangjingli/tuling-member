import { axios } from '@/libs/api.request'

// #region
export const SearchFinance = form => {
  return axios.post('api/Finance/SearchFinance', form)
}

export const SaveFinance = form => {
  return axios.post('api/Finance/SaveFinance', form)
}
export const GetFinance = id => {
  return axios.get('api/Finance/GetFinance/' + id)
}

export const DeleteFinance = id => {
  return axios.post('api/Finance/DeleteFinance/' + id)
}

// #endregion

export const GetFinanceItem = type => {
  return axios.get('api/Finance/GetFinanceItem/' + type)
}

export const SaveFinanceItem = form => {
  return axios.post('api/Finance/SaveFinanceItem', form)
}

export const DeleteFinanceItem = id => {
  return axios.post('api/Finance/DeleteFinanceItem/' + id)
}
