# TulingMember

#### 介绍
使用.net6，基于 Furion +viewui开发的一套极简的进销存管理系统。

#### 技术栈 
1.  sqlserver2019
2.  redis
3.  vue
4.  C#语言

#### 功能点

1.  角色权限
2.  商品管理
3.  销售单
4.  采购单
5.  库存盘点
6.  财务记账
7.  打印
8.  审计日子
9.  预留saas字段，可自行拓展多租户。

#### 使用说明

*   需要了解furion框架，文档地址：https://dotnetchina.gitee.io/furion/
*   熟悉vue,iview

*   设置自己的数据库连接，使用codefirst模式,生成初始数据
1.  Add-Migration v1.0.0 -Context DefaultDbContext
2.  Update-Database
*   也可以使用提供的脚本、或数据库备份文件直接还原 
#### 项目截图
![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/163809_d2fd3fd1_1351955.png "0.png")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/163827_11461954_1351955.png "1.png")
![![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/163843_9186cb42_1351955.png "2.png")]
![![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/163853_6ed27c47_1351955.png "3.png")]
![输入图片说明](https://images.gitee.com/uploads/images/2022/0711/163940_b5af64b5_1351955.png "4.png")
 
#### 演示地址

*   http://jxc.123store.top/ 
*   账号：admin 密码：123456
 

#### 其他

如无需要无功能基础框架，可使用，https://gitee.com/a106_admin/joy-admin


#### 关于作者

*   微信：yy1061800 QQ:1063721800 欢迎各位同学相互交流、共同进步。
*   接受个性化定制开发,高质量服务（软件开发、小程序、app、网站等）
