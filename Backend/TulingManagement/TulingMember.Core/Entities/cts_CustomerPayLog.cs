﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace TulingMember.Core
{
    /// <summary>
    /// 。
    /// </summary> 
    public class cts_CustomerPayLog : DEntityTenant
    {

        /// <summary>
        /// 。
        /// </summary>

        public OrderType OrderType { get; set; }
        /// <summary>
        /// 。
        /// </summary>

        public long OrderId { get; set; }
        /// <summary>
        /// 。
        /// </summary>

        public string OrderNo { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public DateTime? OrderDate { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public long CustomerId { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public string CustomerName { get; set; }

    
        /// <summary>
        /// 合计。
        /// </summary>
     
        public decimal OldAmount { get; set; }

    
        /// <summary>
        ///  
        /// </summary>
     
        public decimal ChangeAmount { get; set; }
        /// <summary>
        ///  
        /// </summary>

        public decimal DiscountAmount { get; set; }

        /// <summary>
        /// 。
        /// </summary>

        public decimal NewAmount { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public string Remark { get; set; }

    }
}