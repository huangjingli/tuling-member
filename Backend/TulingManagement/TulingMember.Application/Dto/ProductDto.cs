﻿using Mapster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TulingMember.Core;

namespace TulingMember.Application
{
    public class ProductDto :BaseEntityDto
    {

       
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 重量
        /// </summary>
        public decimal Weight { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string Specification { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public decimal Num { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }

        public long TypeId { get; set; }
        public string TypeName { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 采购价格
        /// </summary>
        public decimal InPrice { get; set; }
        /// <summary>
        /// 划线价格
        /// </summary>
        public decimal FakePrice { get; set; }
         
    }
}
