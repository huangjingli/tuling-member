﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TulingMember.Application.Dto
{
    public class SaleAmountDto
    {
        public long? ProductId { get; set; }
        public string ProductName { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerName { get;  set; }
        public long? TenantId { get;  set; }
        public bool IsDeleted { get;  set; }
        public decimal? Weight { get;  set; }
        public decimal? Amount { get;  set; }
        public decimal? AllFee { get; set; }
    }
    public class SaleAmountByDateDto
    {
        public string OrderDate { get; set; } 
        public decimal AllFee { get; set; }
        public int OrderCount { get; set; }
    }
}
